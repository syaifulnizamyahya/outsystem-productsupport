using System;
using System.Linq;
using Xunit;

//4. Using any programming language you want, write two different
//implementations(one that uses a loop and one that does not) of a
//function that receives a string and returns the sum of the
//numeric characters in it.
//(Example: input - �0ut5yst3m5�; result - 13)
//Provide instructions on how to test your code including calls to 
//your function with input examples.Your instructions will be followed 
//thoroughly - If we cannot test your code by following them, you 
//have failed the question.The simpler the better.

public class Sum
{
    /// <summary>
    /// Receives a string and returns the sum of the numeric characters in it
    /// </summary>
    /// <param name="inputString"></param>
    /// <returns></returns>
    public static int SumOfNumericCharactersLoop(string inputString)
    {
        // find digits in string and put into array of chars
        var digit = inputString.Where(Char.IsDigit).ToArray();
        int sum = 0;
        foreach (var item in digit)
        {
            // for each chars in array, convert to int and sum 
            sum += Convert.ToInt32(char.GetNumericValue(item));
        }
        return sum;
    }

    public static int SumOfNumericCharacters(string inputString)
    {
        // for each char in string, check if its digits. 
        // if it is a digit, convert to int and return it. 
        // if its not a digit, return 0
        // sum the return values
        var sum = inputString
            .Select(c => Char.IsDigit(c) ? Convert.ToInt32(c.ToString()) : 0)
            .Sum();
        return sum;
    }

}

namespace XUnitTestProject
{
    public class Question4
    {
        public class SumOfNumericCharacters
        {
            [Fact]
            public void When_InputStringsWithNumber_Expect_SumOfNumber()
            {
                //Arrange
                string input = "0ut5yst3m5";

                //Act 
                int results = Sum.SumOfNumericCharacters(input);

                //Assert
                Assert.Equal(13, results);
            }
        }
        public class SumOfNumericCharactersLoop
        {
            [Fact]
            public void When_InputStringsWithNumber_Expect_SumOfNumber()
            {
                //Arrange
                string input = "0ut5yst3m5";

                //Act 
                int results = Sum.SumOfNumericCharactersLoop(input);

                //Assert
                Assert.Equal(13, results);
            }
        }
    }
}
