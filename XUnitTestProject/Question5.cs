﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;


//5. The following code for the function calculateSum(written in C#) is 
//supposed to calculate the sum of all integer values from 0 to n. However, 
//it has a bug. What is expected from you is:
//1. Fix the bug the code.
//2. Explain the steps you took to find the bug
//3. Return a piece of code where the function is called with several different 
//values and the result is printed.
//public static int calculateSum(int n)
//{
//    int totalSum = n;
//    for (int i = 1; i > n; i--)
//        totalSum += i;
//    return (totalSum);
//}

public class CalculateSum
{
    public static int calculateSum(int n)
    {
        int totalSum = n;
        for (int i = 1; i > n; i--)
            totalSum += i;
        return (totalSum);
    }

    /// <summary>
    /// Calculate the sum of all integer values from 0 to n
    /// </summary>
    /// <param name="n">Number of integer to count</param>
    /// <returns>Return sums</returns>
    public static int fixedCalculateSum(int n)
    {
        // My debugging steps 
        //1. Compile and run and check for error.
        //2. Add breakpoints and step through the code
        //3. Fix the bug
        //4. Add parameterised test to further verify correctness

        if (n < 0)
        {
            return -1;
        }
        uint totalSum = Convert.ToUInt32(n);
        for (uint i = 0; i < n; i++)
        {
            totalSum += i;
            if (totalSum > int.MaxValue)
            {
                // if the sum is more than int range, return -1;
                return -1;
            }
        }
        return (Convert.ToInt32(totalSum));
    }
}


namespace XUnitTestProject
{
    public class Question5
    {
        public class calculateSum
        {
            [Fact]
            public void When_InputNumber_Expect_SumOfNumber()
            {
                //Arrange
                var input = 11;

                //Act 
                int results = CalculateSum.calculateSum(input);

                //Assert
                Assert.Equal(66, results);
            }
        }
        public class fixedCalculateSum
        {
            [Theory]
            [InlineData(0, 0)]
            [InlineData(1, 1)]
            [InlineData(11, 66)]
            [InlineData(int.MaxValue, -1)]
            [InlineData(-1, -1)]
            public void When_InputNumber_Expect_SumOfNumber(int input, int expected)
            {
                //Arrange

                //Act 
                int results = CalculateSum.fixedCalculateSum(input);

                //Assert
                Assert.Equal(expected, results);
            }
        }
    }
}
